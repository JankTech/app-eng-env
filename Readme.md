# App Eng Env 

App Eng Env is a Go-based CLI tool. It creates temporary app.yaml files for App Engine CI/CD pipelines, sourcing sensitive environment variables from the system. It works with a template YAML file, allowing safe version control, and produces a finalized app.yaml by incorporating sensitive system variables.

## Features

- Read an app.yaml file and append environment variables
- Command-line interface for easy integration with your existing build and deployment pipeline
- Customizable environment variable prefix

## Installation

Binaries are available from the [latest release](https://gitlab.com/JankTech/app-eng-env/-/releases/permalink/latest) page.


## Build from source

To build AEE from source, you'll need to have Go installed on your system. You can then clone the repository and build the application:

```bash
git clone https://gitlab.com/JankTech/app-eng-env.git
cd app-eng-env
go build -o ./bin/aee main.go
```

## Usage

Available flags:
```
  -i, --infile string    The name of the template file (default "app.template.yaml")
  -o, --outfile string   The name of the output file, defaults to stdout
  -p, --prefix string    Environment variable prefix (default "APP_")
  -s, --service string   Change the service name in the yaml
  --help                 Shows this help message
```

Example usage:
```bash
aee -i path/to/your/template.yaml -p ENV_PREFIX_
```
This command takes all system variables prefixed with `ENV_PREFIX_` (e.g. `ENV_PREFIX_GOOGLE_CLIENT_SECRET`), removes the `ENV_PREFIX_` from the names, and outputs a merged version of `template.yaml` with the additional environment variables to stdout.

This strategy allows you to commit the `template.yaml` to your source control without including any secrets. You can then add the secrets to your CI/CD pipeline config, inject them into a temporary `app.yaml` for a single deployment execution, and delete the `app.yaml` file once the `gcloud` deployment is complete.

## Contributing

Contributions are welcome! Please feel free to submit a Pull Request.

## License

This project is licensed under the MIT License.

