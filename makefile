PROJECT_NAME := "app-eng-env"
BIN_NAME := "aee"
PKG := "gitlab.com/janktech/$(PROJECT_NAME)"

.PHONY: all dep build clean

all: build

dep: ## Get the dependencies
	@go get -v -d ./...

build: dep ## Build the binary file
	@GOOS=linux GOARCH=amd64 go build -v -o ./bin/aee_linux_x64 $(PKG)
	@GOOS=linux GOARCH=arm64 go build -v -o ./bin/aee_linux_arm64 $(PKG)
	@GOOS=linux GOARCH=386   go build -v -o ./bin/aee_linux_x86 $(PKG)
	@GOOS=linux GOARCH=arm    go build -v -o ./bin/aee_linux_arm $(PKG)

	@GOOS=windows GOARCH=amd64 go build -v -o ./bin/aee_win_x64 $(PKG)
	@GOOS=windows GOARCH=arm64 go build -v -o ./bin/aee_win_arm64 $(PKG)
	@GOOS=windows GOARCH=386   go build -v -o ./bin/aee_win_x86 $(PKG)
	@GOOS=windows GOARCH=arm   go build -v -o ./bin/aee_win_arm $(PKG)
	
	@GOOS=darwin GOARCH=amd64  go build -v -o ./bin/aee_macos_x64 $(PKG)
	@GOOS=darwin GOARCH=arm64  go build -v -o ./bin/aee_macos_arm64 $(PKG)

clean: ## Remove previous build
	@rm -f ./bin/${BIN_NAME}

help: ## Display this help message
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
