package main

import (
	"fmt"
	"os"
	"strings"

	"github.com/spf13/pflag"
	"gopkg.in/yaml.v3"
)

func main() {
	var infile string
	var prefix string
	var service string
	var outfile string

	pflag.StringVarP(&infile, "infile", "i", "app.template.yaml", "The name of the template file")
	pflag.StringVarP(&prefix, "prefix", "p", "APP_", "Environment variable prefix")
	pflag.StringVarP(&service, "service", "s", "", "Change the service name in the yaml")
	pflag.StringVarP(&outfile, "outfile", "o", "", "The name of the output file, defaults to stdout")
	pflag.Parse()

	app, err := parseYAML(infile)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	envVars := getEnvVars(app, prefix)

	output, err := buildYAML(app, envVars, service)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	if outfile != "" {
		err = os.WriteFile(outfile, []byte(output), 0755)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		os.Exit(0)
	}

	// output to stdout if no outfile specified
	fmt.Println(string(output))
}

func parseYAML(file string) (map[string]any, error) {
	input, err := os.ReadFile(file)
	if err != nil {
		if !os.IsNotExist(err) {
			panic(err)
		}
		input = []byte{}
	}

	app := map[string]any{}
	err = yaml.Unmarshal(input, &app)
	if err != nil {
		return app, err
	}

	if app == nil {
		app = make(map[string]any)
	}

	return app, nil
}

func getEnvVars(app map[string]any, prefix string) map[string]any {
	envVars, ok := app["env_variables"].(map[string]any)
	if !ok {
		envVars = make(map[string]any)
		app["env_variables"] = envVars
	}

	for _, e := range os.Environ() {
		pair := strings.SplitN(e, "=", 2)
		if strings.HasPrefix(pair[0], prefix) {
			key := strings.TrimPrefix(pair[0], prefix)
			envVars[key] = pair[1]
		}
	}

	return envVars
}

func buildYAML(app map[string]any, envVars map[string]any, service string) (string, error) {
	if envVars != nil {
		app["env_variables"] = envVars
	}

	if service != "" {
		app["service"] = service
	}

	output, err := yaml.Marshal(app)
	if err != nil {
		return "", err
	}

	return string(output), nil
}
